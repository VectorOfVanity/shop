/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var path = require('path');
module.exports = {


  delete : function (req,res) {

    var id = req.param('id');

    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    Product.destroy({id : id}).exec(function (err) {

      if (err) {
        response.code = 401;
        response.message = err.message;
        return res.ok(response);
      }

      response.hasError = false;
      response.code = 200;

      return res.ok(response);

    });

  },

  buy : function (req,res) {
    var id = req.param('id');
    var user = req.param('userid');

    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    Product.findOne({id : id}).exec(function (err, product) {
      if (err) {
        response.code = 401;
        response.message = err.message;
        return res.ok(response);
      };

      if(!product){
        response.code = 401;
        response.message = "The product does not exist";
        return res.ok(response);
      }

      Bought.create({product : product.id , user : user}).exec(function (err, records) {
        if (err) {
          response.code = 401;
          response.message = err.message;
          return res.ok(response);
        };

        response.hasError = false;
        response.code = 200;
        return res.ok(response);
      });


    });

  },

  add : function (req,res) {
    var product = req.allParams();

    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    Product.create(product).exec(function (err, records) {
      if (err) {
        response.code = 401;
        response.message = err.message;;
        return res.ok(response);
      };

      response.hasError = false;
      response.code = 200;
      response.product = records;
      return res.ok(response);
    });
  },

  uploadFile: function(req,res,next){
    var id = req.param('id');


    var reqFile = req.file('file');

    if (reqFile._files.length != 0) {
      req.file('file').upload({
        // don't allow the total upload size to exceed ~10MB
        dirname: '../../assets/images/product/'
      }, function whenDone(err, uploadedFiles) {
        if (err) {
          console.log(err);
          return;
        }
        // If no files were uploaded, respond with an error.
        if (uploadedFiles.length === 0) {
          console.log('No file was uploaded');
          return;
        }
        var url = '/images/product/' + uploadedFiles[0].fd.substring(uploadedFiles[0].fd.lastIndexOf(path.sep) + 1);


        Product.update({id: id},{url: url}).exec(function afterwards(err, updated){

          if (err) {
            // handle error here- e.g. `res.serverError(err);`
            return;
          }


          return res.ok();

        });

      });
    } else {
      reqFile.upload({noop: true});
      return res.ok();
    }

  },


  list : function (req,res) {
    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    Product.find().populate("category").populate("subcategory").exec(function (err, products) {
      if (err) {
        response.code = 401;
        response.message = err.message;;
        return res.ok(response);
      };

      response.hasError = false;
      response.code = 200;
      response.products = products;
      return res.ok(response);
    });

  }


};

