/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	"new" : function (req, res){
    res.view();
  },

  "login" : function (req, res){
    res.view();
  },

  "panel" : function (req, res){
    res.view();
  },

  create : function(req,res,next){
    var user = req.allParams();

    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    user.role = 'user';
    User.create(user).exec(function(err,user){
      if (err) {
        response.code = 401;
        response.message = err.message;;
        return res.ok(response);
      };

      response.user = user;
      response.hasError = false;
      response.code = 200;
      return res.ok(response);

    });
  },

  logout: function (req, res) {
    if (req.session.User != undefined) {
      req.session.destroy(function (err) {
        return res.redirect('/user/login/');
      });
    } else {
      return res.notFound();
    }

  },

  emailcheck: function (req, res, next) {

    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    User.findOne({email: req.param('email')}, function foundUser(err, user) {
      if (err) {
        response.code = 401;
        response.message = err.message;
        return res.ok(response);
      }
      if (user != undefined) {
        var emailError = [{name: 'NoAccount', message: 'The email address ' + req.param('email') + 'founded'}];
        response.message = 'The email address ' + req.param('email') + 'founded';
        response.code = 402;
        return res.ok(response);
      } else {
        response.hasError = false;
        response.code = 200;
        return res.ok(response);
      }
    });
  },

  submitLogin : function (req, res) {

    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    User.findOne({email: req.allParams().email}, function foundUser(err, user) {
      if (err) {
        response.code = 401;
        response.message = err.code;
        return res.ok(response);
      }

      if (!user) {
        var emailError = [{
          name: 'NoAccount',
          message: 'The email address' + req.allParams().email  + ' could not be found'
        }];

        response.code = 401;
        response.message = 'The email address ' + req.allParams().email  + ' could not be found';
        return res.ok(response);
      }

      if(user.password !== req.allParams().password){
        var usernamePasswordMismatchError = [{
          name: 'UsernamePasswordMismatchError',
          message: 'Invalid combination of Username and password'
        }];
        response.code = 401;
        response.message = 'Invalid combination of Username and password';
        return res.ok(response);
      }
      req.session.authenticated = true;
      req.session.User = user;
      req.session.successLogin = true;
      response.code = 200;
      response.hasError = false;
      response.user = user;
      return res.ok(response);
    });
  }

};

