/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


  delete : function (req,res) {

    var id = req.param('id');

    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    Bought.destroy({id : id}).exec(function (err) {

      if (err) {
        response.code = 401;
        response.message = err.message;
        return res.ok(response);
      }

      response.hasError = false;
      response.code = 200;

      return res.ok(response);

    });

  },


  list : function (req,res) {

    var id = req.param('id');

    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    Bought.find({user : id}).populate("product").exec(function (err, ps) {
      if (err) {
        response.code = 401;
        response.message = err.message;
        return res.ok(response);
      }

      var fullProducts = [];

      if(ps.length === 0){
        response.code = 200;
        response.hasError = false;
        response.products = [];
        return res.ok(response);
      }

      ps.forEach(function (p) {
        getCategorySubCategory(p.product,p.id,fullProducts,ps.length,response,res);
      });


    });

  }


};


function getCategorySubCategory(product, id,fullProducts, length, response , res) {

  Product.findOne({id: product ? product.id : undefined}).populate("category").populate("subcategory").exec(function (err, product) {

    if (err) {
      response.code = 401;
      response.message = err.message;
      response.products = [];
      return res.ok(response);
    }
    product.order = id;
    fullProducts.push(product);
    if (fullProducts.length == length) {

      response.code = 200;
      response.hasError = false;
      response.products = fullProducts;

      return res.ok(response);
    }


  });

}
