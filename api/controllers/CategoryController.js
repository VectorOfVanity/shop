/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  delete : function (req,res) {

    var id = req.param('id');

    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    Category.destroy({id : id}).exec(function (err) {

      if (err) {
        response.code = 401;
        response.message = err.message;
        return res.ok(response);
      }

      response.hasError = false;
      response.code = 200;

      return res.ok(response);

    });

  },

  add : function (req,res) {
    var name = req.param('category');
    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    Category.create({name : name}).exec(function (err, records) {
      if (err) {
        response.code = 401;
        response.message = err.message;;
        return res.ok(response);
      };

      response.hasError = false;
      response.code = 200;
      response.category = records[0];

      return res.ok(response);
    });

  },

  list : function (req,res) {
    var response = {
      hasError: true,
      code: 0,
      message: ''
    };

    Category.find().exec(function (err, categories) {
      if (err) {
        response.code = 401;
        response.message = err.message;;
        return res.ok(response);
      }

      response.hasError = false;
      response.code = 200;
      response.categories = categories;
      return res.ok(response);
    });

  }


};

