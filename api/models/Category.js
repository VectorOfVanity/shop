/**
 * Created by yura on 25.05.17.
 */


var uuid = require("node-uuid");

module.exports = {
  schema: true,
  attributes: {
    id: {
      type: 'string',
      required: true,
      unique: true,
      primaryKey: true,
      defaultsTo: function () {
        return uuid.v4();
      }
    },

    name : {
      type: 'string'
    }


  }
};
