/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {


    User.find({role : 'admin'}).exec(function (err, admins) {
      if(err){
        console.log("Upps :(");
        return res.badRequest();
      }

      if(admins.length == 0){

        User.create({
            name : "admin",
            surname : "admin",
            email : "admin@gmail.com",
            password : "admin@gmail.com",
            role : "admin"
          }).exec(function (err, records) {
          if(err){
            console.log("upps :(");
          }
          cb();
        });

      }else{
        cb();
      }

    });
};
