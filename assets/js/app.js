/**
 * Created by SAMSUNG on 08.07.2016.
 */


var app = angular.module('tracker',[]);


app.controller('userNewCtrl', function($scope,$http){
  $scope.user = {};
  $scope.create = function() {
    if (registerValidation()) {
      $http.post('/user/create/', $scope.user).success(function (data) {
        if(data.hasError){
          bootbox.alert(data.message, function() {
          });
        }else{
          window.location.href = '/';
        }
      });
    }
  }
});

app.controller('panelCtrl', function($scope,$http){

  $scope.category = {};

  $scope.categories = [];


  $scope.saveCategory = function () {
    $http.post('/category/add/',{category : $scope.category.name}).success(function (data) {
      $http.get('/category/list/').success(function (data) {
        $scope.categories = data.categories;
        $scope.category = {};
      });
    });
  }

  $http.get('/category/list/').success(function (data) {
    $scope.categories = data.categories;
  });


  $scope.subcategory = {};

  $scope.subcategories = [];

  $scope.saveSubcategory = function () {
    $http.post('/subcategory/add/',{subcategory : $scope.subcategory.name}).success(function (data) {
      $http.get('/subcategory/list/').success(function (data) {
        $scope.subcategories = data.subcategories;
        $scope.subcategory = {};
      });
    });
  }


  $http.get('/subcategory/list/').success(function (data) {
    $scope.subcategories = data.subcategories;
  });

  $scope.product = {};
  $scope.products = {};


  $scope.removeProduct = function(id){
    $http.post('/product/delete/',{id : id}).success(function (data) {
      $http.get('/product/list/').success(function (data) {
        $scope.products = data.products;
      });
    });
  };

  $scope.removeCategory = function(id){
    $http.post('/category/delete/',{id : id}).success(function (data) {
      $http.get('/category/list/').success(function (data) {
        $scope.categories = data.categories;
      });
    });
  };

  $scope.removeSubcategory = function(id){
    $http.post('/subcategory/delete/',{id : id}).success(function (data) {
      $http.get('/subcategory/list/').success(function (data) {
        $scope.subcategories = data.subcategories;
      });
    });
  };

  $scope.saveProduct = function () {
    $http.post('/product/add/',$scope.product).success(function (data) {


      var formData = new FormData();
      formData.append('id', data.product.id);
      formData.append('file', $("#photoFile")[0].files[0]);
      $http.post('/product/uploadFile/', formData,{
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      }).success(function (photo) {
        $http.get('/product/list/').success(function (data) {
          $scope.products = data.products;
        });
      });

    });
  }

  $http.get('/product/list/').success(function (data) {
    $scope.products = data.products;
  });


});

app.controller('homeCtrl', function($scope,$http){
  $scope.products = [];


  $scope.myproducts = [];

  $http.get('/product/list/').success(function (data) {
      $scope.products = data.products;
    $http.get('/bought/list/').success(function (data) {
      $scope.myproducts = data.products;
    });
  });

});

app.controller('loginCtrl', function($scope,$http){
  $scope.user = {};
  $scope.loginSubmit = function(){
      $http.post('/user/submitLogin/',$scope.user).success(function (data) {
        if(data.hasError){
          bootbox.alert(data.message, function() {
            $scope.user.password = '';
          });
        }else{
          window.location.href = '/';
        }
      });
  }
});
