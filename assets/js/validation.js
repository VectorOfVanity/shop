/**
 * Created by SAMSUNG on 11.07.2016.
 */

function validateEmail(email){
  email = $.trim(email);

  $("#email").css('background-color','white');
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(email))
    {
      $("#email").css('background-color','rgb(255, 242, 239)');
      return '<li class="error-li">Please enter a valid Email address</li>';
    }
    return "";
}


function checkEmail(callback){
  $("#email").css('background-color','white');
  var formData = new FormData();
  formData.append('email', $("#email").val());
  $.ajax({
    type: 'post',
    url: '/user/emailcheck',
    data: formData,
    async: false,
    processData: false,
    contentType: false,
    success: function () {
      $("#email").css('background-color','white');
      if(callback) callback();
    },
    error: function (data) {
      $("#email").css('background-color','rgb(255, 242, 239)');
      if(callback) callback('<li class="error-li">E-mail address already exists</li>');
    }
  });
}



function registerValidation(){
  var contentText = '';
  var checkEmailContentText = "";

  checkEmail(function(output){
    if(output != undefined)
      checkEmailContentText = output;
  });
  if(checkEmailContentText !== "") contentText += checkEmailContentText;


  var name = $.trim($("#name").val());
  $("#name").css('background-color','white');
  if(name.length <= 0) {
    $("#name").css('background-color','rgb(255, 242, 239)');
    contentText += '<li class="error-li">Your Name cannot be empty</li>';
  }

  var surname = $.trim($("#surname").val());
  $("#surname").css('background-color','white');
  if(surname.length <= 0) {
    $("#surname").css('background-color','rgb(255, 242, 239)');
    contentText += '<li class="error-li">Your Surname cannot be empty</li>';
  }

  var password = $.trim($("#password").val());
  $("#password").css('background-color','white');
  if(password.length <= 5) {
    $("#password").css('background-color','rgb(255, 242, 239)');
    contentText += '<li class="error-li">The password must be at least 6 characters long</li>';
  }

  var confPassword = $.trim($("#confPass").val());
  $("#confPass").css('background-color','white');
  if(password !== confPassword){
    $("#confPass").css('background-color','rgb(255, 242, 239)');
    contentText += '<li class="error-li">The passwords do not match</li>';
  }


  var email = $.trim($("#email").val());
  if(validateEmail(email) != ''){
    contentText += validateEmail(email);
  }

  if (contentText !== '') {
    $('button[data-toggle="popover"]').attr('data-content',contentText).popover('show');;
    return false;
  } else {
    $('button[data-toggle="popover"]').popover('destroy');

  }
  return true;






}
